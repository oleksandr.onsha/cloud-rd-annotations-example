package com.onsha;

import com.onsha.model.User;
import com.onsha.validator.EntityValidator;

/**
 * Created by Oleksandr Onsha on 11/6/18
 */
public class ApplicationEntry {

    public static void main(String[] args) throws IllegalAccessException {
        System.out.println("Checking User1");
        User user1 = new User("User1", new Object(), 22);
        EntityValidator.assertUserIsValid(user1);

//        System.out.println("Checking User2");
//        User user2 = new User("User2", null, 1000);
//        EntityValidator.assertUserIsValid(user2);

        System.out.println("Checking User3");
        User user3 = new User("", null, -100500);
        EntityValidator.assertUserIsValid(user3);

        System.out.println("Check done");
    }

}
