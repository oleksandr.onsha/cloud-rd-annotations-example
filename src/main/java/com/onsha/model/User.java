package com.onsha.model;

import com.onsha.annotation.NotEmpty;
import com.onsha.annotation.NotNull;
import com.onsha.annotation.Range;

/**
 * Created by Oleksandr Onsha on 11/6/18
 */
public class User {

    @NotEmpty
    private String name;

    @NotNull
    private Object address;

    @Range(max = 120) // according to wiki
    private int age;

    public User(String name, Object address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", address=" + address +
                ", age=" + age +
                '}';
    }
}
