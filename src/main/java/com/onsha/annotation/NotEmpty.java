package com.onsha.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that marked String field must not be empty
 *
 * Created by Oleksandr Onsha on 11/6/18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// @Inherited
public @interface NotEmpty {
}
