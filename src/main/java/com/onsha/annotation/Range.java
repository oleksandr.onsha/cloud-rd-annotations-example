package com.onsha.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Oleksandr Onsha on 11/6/18
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Range {
    /**
     * Defines max value of annotated field.
     *
     * @return
     */
    int max() default 100;

    /**
     * Defines min value of annotated field.
     *
     * @return
     */
    int min() default 0;
}
