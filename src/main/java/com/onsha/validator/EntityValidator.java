package com.onsha.validator;

import com.onsha.annotation.NotEmpty;
import com.onsha.annotation.NotNull;
import com.onsha.annotation.Range;
import com.onsha.model.User;
import com.sun.tools.javac.util.List;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Oleksandr Onsha on 11/6/18
 */
public class EntityValidator {

    public static void assertUserIsValid(User user) throws IllegalAccessException {
        Class<? extends User> userClass = user.getClass();
        Map<String, String> errorMap = new HashMap<>();
        for (Field field: userClass.getDeclaredFields()) {
            field.setAccessible(true);
            // check for NotNull
            NotNull notNullAnnotation = field.getAnnotation(NotNull.class);
            if (notNullAnnotation != null) {
                Object o = field.get(user); // too expensive operation to move field extraction
                if (o == null) {
                    errorMap.put(field.getName(), "Null value is not allowed");
                }
            }

            // check for NotEmpty
            NotEmpty notEmptyAnnotation = field.getAnnotation(NotEmpty.class);
            if (notEmptyAnnotation != null) {
                Class<?> type = field.getType();
                if (type == String.class || type == List.class) {
                    String value = (String) field.get(user);
                    if (value == null || value.isEmpty()) {
                        errorMap.put(field.getName(), "Null or empty value is not allowed");
                    }
                } else {
                    throw new RuntimeException("NotEmpty annotation should be used with String or List fields.");
                }
            }

            // check for range validity
            Range annotation = field.getAnnotation(Range.class);
            if (annotation != null) {
                Class<?> type = field.getType();
                if (type != int.class && type != Integer.class) {
                    throw new RuntimeException("Range annotation should be used with int or Integer fields.");
                } else {
                    Integer value = (Integer) field.get(user);
                    if (annotation.min() > value) {
                        errorMap.put(field.getName(), "The field has value less that Range's minimum value. Min is " + annotation.min());
                    }
                    if (annotation.max() < value) {
                        errorMap.put(field.getName(), "The field has value greater that Range's maximum value. Max is " + annotation.min());
                    }
                }

            }
        }
        if (!errorMap.isEmpty()) {
            throw new IllegalArgumentException(errorMap.entrySet().stream()
                    .map(entry -> "Field '" + entry.getKey() + "' is invalid: " + entry.getValue() + "; ")
                    .collect(Collectors.joining()));
        }
    }

}
